/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./mirage.js":
/*!*******************!*\
  !*** ./mirage.js ***!
  \*******************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"makeServer\": () => (/* binding */ makeServer)\n/* harmony export */ });\n/* harmony import */ var faker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! faker */ \"faker\");\n/* harmony import */ var faker__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(faker__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var miragejs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! miragejs */ \"miragejs\");\n/* harmony import */ var miragejs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(miragejs__WEBPACK_IMPORTED_MODULE_1__);\n\n\nfaker__WEBPACK_IMPORTED_MODULE_0___default().seed(5);\nfunction makeServer({ environment =\"test\"  } = {}) {\n    let server = (0,miragejs__WEBPACK_IMPORTED_MODULE_1__.createServer)({\n        environment,\n        timing: 750,\n        routes () {\n            this.namespace = \"api\";\n            this.get(\"/checking\", ()=>{\n                // Force an error\n                return new miragejs__WEBPACK_IMPORTED_MODULE_1__.Response(500);\n                return {\n                    stat: \"$8,027\",\n                    change: \"$678\",\n                    changeType: \"increase\"\n                };\n            }, {\n                timing: 500\n            });\n            this.get(\"/savings\", ()=>{\n                // Force an error\n                // return new Response(500);\n                return {\n                    stat: \"$24,581\",\n                    change: \"$1,167\",\n                    changeType: \"decrease\"\n                };\n            }, {\n                timing: 1500\n            });\n            this.get(\"/credit\", ()=>{\n                return {\n                    stat: \"$4,181\",\n                    change: \"$412\",\n                    changeType: \"increase\"\n                };\n            }, {\n                timing: 750\n            });\n            this.namespace = \"\";\n            this.passthrough();\n        }\n    });\n    // Don't log passthrough\n    server.pretender.passthroughRequest = ()=>{};\n    server.logging = false;\n    return server;\n}\nlet isClient = \"undefined\" !== \"undefined\";\nif (isClient && !window.server) {\n    window.server = makeServer({\n        environment: \"development\"\n    });\n}\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9taXJhZ2UuanMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBeUI7QUFDd0I7QUFFakRBLGlEQUFVLENBQUMsQ0FBQztBQUVMLFNBQVNJLFVBQVUsQ0FBQyxDQUFDLENBQUNDLFdBQVcsRUFBRyxDQUFNLE9BQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDekQsR0FBRyxDQUFDQyxNQUFNLEdBQUdMLHNEQUFZLENBQUMsQ0FBQztRQUN6QkksV0FBVztRQUVYRSxNQUFNLEVBQUUsR0FBRztRQUVYQyxNQUFNLElBQUcsQ0FBQztZQUNSLElBQUksQ0FBQ0MsU0FBUyxHQUFHLENBQUs7WUFFdEIsSUFBSSxDQUFDQyxHQUFHLENBQ04sQ0FBVyxnQkFDTCxDQUFDO2dCQUNMLEVBQWlCO2dCQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDUiw4Q0FBUSxDQUFDLEdBQUc7Z0JBRXZCLE1BQU0sQ0FBQyxDQUFDO29CQUNOUyxJQUFJLEVBQUUsQ0FBUTtvQkFDZEMsTUFBTSxFQUFFLENBQU07b0JBQ2RDLFVBQVUsRUFBRSxDQUFVO2dCQUN4QixDQUFDO1lBQ0gsQ0FBQyxFQUNELENBQUM7Z0JBQUNOLE1BQU0sRUFBRSxHQUFHO1lBQUMsQ0FBQztZQUdqQixJQUFJLENBQUNHLEdBQUcsQ0FDTixDQUFVLGVBQ0osQ0FBQztnQkFDTCxFQUFpQjtnQkFDakIsRUFBNEI7Z0JBRTVCLE1BQU0sQ0FBQyxDQUFDO29CQUNOQyxJQUFJLEVBQUUsQ0FBUztvQkFDZkMsTUFBTSxFQUFFLENBQVE7b0JBQ2hCQyxVQUFVLEVBQUUsQ0FBVTtnQkFDeEIsQ0FBQztZQUNILENBQUMsRUFDRCxDQUFDO2dCQUFDTixNQUFNLEVBQUUsSUFBSTtZQUFDLENBQUM7WUFHbEIsSUFBSSxDQUFDRyxHQUFHLENBQ04sQ0FBUyxjQUNILENBQUM7Z0JBQ0wsTUFBTSxDQUFDLENBQUM7b0JBQ05DLElBQUksRUFBRSxDQUFRO29CQUNkQyxNQUFNLEVBQUUsQ0FBTTtvQkFDZEMsVUFBVSxFQUFFLENBQVU7Z0JBQ3hCLENBQUM7WUFDSCxDQUFDLEVBQ0QsQ0FBQztnQkFBQ04sTUFBTSxFQUFFLEdBQUc7WUFBQyxDQUFDO1lBR2pCLElBQUksQ0FBQ0UsU0FBUyxHQUFHLENBQUU7WUFDbkIsSUFBSSxDQUFDSyxXQUFXO1FBQ2xCLENBQUM7SUFDSCxDQUFDO0lBRUQsRUFBd0I7SUFDeEJSLE1BQU0sQ0FBQ1MsU0FBUyxDQUFDQyxrQkFBa0IsT0FBUyxDQUFDLENBQUM7SUFFOUNWLE1BQU0sQ0FBQ1csT0FBTyxHQUFHLEtBQUs7SUFFdEIsTUFBTSxDQUFDWCxNQUFNO0FBQ2YsQ0FBQztBQUVELEdBQUcsQ0FBQ1ksUUFBUSxHQUFHLENBQWEsZUFBSyxDQUFXO0FBQzVDLEVBQUUsRUFBRUEsUUFBUSxLQUFLQyxNQUFNLENBQUNiLE1BQU0sRUFBRSxDQUFDO0lBQy9CYSxNQUFNLENBQUNiLE1BQU0sR0FBR0YsVUFBVSxDQUFDLENBQUM7UUFBQ0MsV0FBVyxFQUFFLENBQWE7SUFBQyxDQUFDO0FBQzNELENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8yMDIyLTAxLTIxLXN1c3BlbnNlLWVycm9yLWJvdW5kYXJpZXMvLi9taXJhZ2UuanM/Y2E2NyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZmFrZXIgZnJvbSBcImZha2VyXCI7XG5pbXBvcnQgeyBjcmVhdGVTZXJ2ZXIsIFJlc3BvbnNlIH0gZnJvbSBcIm1pcmFnZWpzXCI7XG5cbmZha2VyLnNlZWQoNSk7XG5cbmV4cG9ydCBmdW5jdGlvbiBtYWtlU2VydmVyKHsgZW52aXJvbm1lbnQgPSBcInRlc3RcIiB9ID0ge30pIHtcbiAgbGV0IHNlcnZlciA9IGNyZWF0ZVNlcnZlcih7XG4gICAgZW52aXJvbm1lbnQsXG5cbiAgICB0aW1pbmc6IDc1MCxcblxuICAgIHJvdXRlcygpIHtcbiAgICAgIHRoaXMubmFtZXNwYWNlID0gXCJhcGlcIjtcblxuICAgICAgdGhpcy5nZXQoXG4gICAgICAgIFwiL2NoZWNraW5nXCIsXG4gICAgICAgICgpID0+IHtcbiAgICAgICAgICAvLyBGb3JjZSBhbiBlcnJvclxuICAgICAgICAgIHJldHVybiBuZXcgUmVzcG9uc2UoNTAwKTtcblxuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBzdGF0OiBcIiQ4LDAyN1wiLFxuICAgICAgICAgICAgY2hhbmdlOiBcIiQ2NzhcIixcbiAgICAgICAgICAgIGNoYW5nZVR5cGU6IFwiaW5jcmVhc2VcIixcbiAgICAgICAgICB9O1xuICAgICAgICB9LFxuICAgICAgICB7IHRpbWluZzogNTAwIH1cbiAgICAgICk7XG5cbiAgICAgIHRoaXMuZ2V0KFxuICAgICAgICBcIi9zYXZpbmdzXCIsXG4gICAgICAgICgpID0+IHtcbiAgICAgICAgICAvLyBGb3JjZSBhbiBlcnJvclxuICAgICAgICAgIC8vIHJldHVybiBuZXcgUmVzcG9uc2UoNTAwKTtcblxuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBzdGF0OiBcIiQyNCw1ODFcIixcbiAgICAgICAgICAgIGNoYW5nZTogXCIkMSwxNjdcIixcbiAgICAgICAgICAgIGNoYW5nZVR5cGU6IFwiZGVjcmVhc2VcIixcbiAgICAgICAgICB9O1xuICAgICAgICB9LFxuICAgICAgICB7IHRpbWluZzogMTUwMCB9XG4gICAgICApO1xuXG4gICAgICB0aGlzLmdldChcbiAgICAgICAgXCIvY3JlZGl0XCIsXG4gICAgICAgICgpID0+IHtcbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgc3RhdDogXCIkNCwxODFcIixcbiAgICAgICAgICAgIGNoYW5nZTogXCIkNDEyXCIsXG4gICAgICAgICAgICBjaGFuZ2VUeXBlOiBcImluY3JlYXNlXCIsXG4gICAgICAgICAgfTtcbiAgICAgICAgfSxcbiAgICAgICAgeyB0aW1pbmc6IDc1MCB9XG4gICAgICApO1xuXG4gICAgICB0aGlzLm5hbWVzcGFjZSA9IFwiXCI7XG4gICAgICB0aGlzLnBhc3N0aHJvdWdoKCk7XG4gICAgfSxcbiAgfSk7XG5cbiAgLy8gRG9uJ3QgbG9nIHBhc3N0aHJvdWdoXG4gIHNlcnZlci5wcmV0ZW5kZXIucGFzc3Rocm91Z2hSZXF1ZXN0ID0gKCkgPT4ge307XG5cbiAgc2VydmVyLmxvZ2dpbmcgPSBmYWxzZTtcblxuICByZXR1cm4gc2VydmVyO1xufVxuXG5sZXQgaXNDbGllbnQgPSB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiO1xuaWYgKGlzQ2xpZW50ICYmICF3aW5kb3cuc2VydmVyKSB7XG4gIHdpbmRvdy5zZXJ2ZXIgPSBtYWtlU2VydmVyKHsgZW52aXJvbm1lbnQ6IFwiZGV2ZWxvcG1lbnRcIiB9KTtcbn1cbiJdLCJuYW1lcyI6WyJmYWtlciIsImNyZWF0ZVNlcnZlciIsIlJlc3BvbnNlIiwic2VlZCIsIm1ha2VTZXJ2ZXIiLCJlbnZpcm9ubWVudCIsInNlcnZlciIsInRpbWluZyIsInJvdXRlcyIsIm5hbWVzcGFjZSIsImdldCIsInN0YXQiLCJjaGFuZ2UiLCJjaGFuZ2VUeXBlIiwicGFzc3Rocm91Z2giLCJwcmV0ZW5kZXIiLCJwYXNzdGhyb3VnaFJlcXVlc3QiLCJsb2dnaW5nIiwiaXNDbGllbnQiLCJ3aW5kb3ciXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./mirage.js\n");

/***/ }),

/***/ "./src/pages/_app.js":
/*!***************************!*\
  !*** ./src/pages/_app.js ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ App)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ \"next/head\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! swr */ \"swr\");\n/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var tailwindcss_tailwind_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tailwindcss/tailwind.css */ \"./node_modules/tailwindcss/tailwind.css\");\n/* harmony import */ var tailwindcss_tailwind_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(tailwindcss_tailwind_css__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _mirage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../mirage */ \"./mirage.js\");\n\n\n\n\n\n\nlet isClient = \"undefined\" !== \"undefined\";\nfunction App(props) {\n    let { 0: isFirstRender , 1: setIsFirstRender  } = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)(true);\n    (0,react__WEBPACK_IMPORTED_MODULE_2__.useEffect)(()=>{\n        setIsFirstRender(false);\n    }, []);\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {\n                children:  true && /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"script\", {\n                    dangerouslySetInnerHTML: {\n                        __html: noOverlayWorkaroundScript\n                    }\n                }, void 0, false, {\n                    fileName: \"/Users/twang375/Desktop/Training/xt-frontend-react/suspense/src/pages/_app.js\",\n                    lineNumber: 20,\n                    columnNumber: 11\n                }, this)\n            }, void 0, false, {\n                fileName: \"/Users/twang375/Desktop/Training/xt-frontend-react/suspense/src/pages/_app.js\",\n                lineNumber: 18,\n                columnNumber: 7\n            }, this),\n            isClient && !isFirstRender ? /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(swr__WEBPACK_IMPORTED_MODULE_3__.SWRConfig, {\n                value: {\n                    fetcher: (...args)=>fetch(...args).then((res)=>{\n                            if (res.ok) {\n                                return res.json();\n                            } else {\n                                throw new Error(\"Fetch failed\");\n                            }\n                        })\n                },\n                children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(AppInner, {\n                    ...props\n                }, void 0, false, {\n                    fileName: \"/Users/twang375/Desktop/Training/xt-frontend-react/suspense/src/pages/_app.js\",\n                    lineNumber: 39,\n                    columnNumber: 11\n                }, this)\n            }, void 0, false, {\n                fileName: \"/Users/twang375/Desktop/Training/xt-frontend-react/suspense/src/pages/_app.js\",\n                lineNumber: 27,\n                columnNumber: 9\n            }, this) : null\n        ]\n    }, void 0, true));\n};\nfunction AppInner({ Component , pageProps  }) {\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        className: \"flex justify-center w-full min-h-screen pt-8 antialiased xs:px-20 sm:px-10 md:pt-28 bg-slate-100\",\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n            className: \"w-full max-w-sm px-8\",\n            children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n                ...pageProps\n            }, void 0, false, {\n                fileName: \"/Users/twang375/Desktop/Training/xt-frontend-react/suspense/src/pages/_app.js\",\n                lineNumber: 50,\n                columnNumber: 9\n            }, this)\n        }, void 0, false, {\n            fileName: \"/Users/twang375/Desktop/Training/xt-frontend-react/suspense/src/pages/_app.js\",\n            lineNumber: 49,\n            columnNumber: 7\n        }, this)\n    }, void 0, false, {\n        fileName: \"/Users/twang375/Desktop/Training/xt-frontend-react/suspense/src/pages/_app.js\",\n        lineNumber: 48,\n        columnNumber: 5\n    }, this));\n}\nconst noOverlayWorkaroundScript = `\n  window.addEventListener('error', event => {\n    event.stopImmediatePropagation()\n  })\n\n  window.addEventListener('unhandledrejection', event => {\n    event.stopImmediatePropagation()\n  })\n`;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvcGFnZXMvX2FwcC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQTRCO0FBQ2U7QUFDWjtBQUNFO0FBQ1o7QUFFckIsR0FBRyxDQUFDSSxRQUFRLEdBQUcsQ0FBYSxlQUFLLENBQVc7QUFFN0IsUUFBUSxDQUFDQyxHQUFHLENBQUNDLEtBQUssRUFBRSxDQUFDO0lBQ2xDLEdBQUcsTUFBRUMsYUFBYSxNQUFFQyxnQkFBZ0IsTUFBSU4sK0NBQVEsQ0FBQyxJQUFJO0lBRXJERCxnREFBUyxLQUFPLENBQUM7UUFDZk8sZ0JBQWdCLENBQUMsS0FBSztJQUN4QixDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBRUwsTUFBTTs7d0ZBRURSLGtEQUFJOzBCQWpCWCxLQWtCOEMsZ0ZBQ25DUyxDQUFNO29CQUNMQyx1QkFBdUIsRUFBRSxDQUFDO3dCQUFDQyxNQUFNLEVBQUVDLHlCQUF5QjtvQkFBQyxDQUFDOzs7Ozs7Ozs7OztZQUtuRVIsUUFBUSxLQUFLRyxhQUFhLCtFQUN4QkosMENBQVM7Z0JBQ1JVLEtBQUssRUFBRSxDQUFDO29CQUNOQyxPQUFPLE1BQU1DLElBQUksR0FDZkMsS0FBSyxJQUFJRCxJQUFJLEVBQUVFLElBQUksRUFBRUMsR0FBRyxHQUFLLENBQUM7NEJBQzVCLEVBQUUsRUFBRUEsR0FBRyxDQUFDQyxFQUFFLEVBQUUsQ0FBQztnQ0FDWCxNQUFNLENBQUNELEdBQUcsQ0FBQ0UsSUFBSTs0QkFDakIsQ0FBQyxNQUFNLENBQUM7Z0NBQ04sS0FBSyxDQUFDLEdBQUcsQ0FBQ0MsS0FBSyxDQUFDLENBQWM7NEJBQ2hDLENBQUM7d0JBQ0gsQ0FBQztnQkFDTCxDQUFDO3NHQUVBQyxRQUFRO3VCQUFLaEIsS0FBSzs7Ozs7Ozs7Ozt1QkFFbkIsSUFBSTs7O0FBR2QsQ0FBQztTQUVRZ0IsUUFBUSxDQUFDLENBQUMsQ0FBQ0MsU0FBUyxHQUFFQyxTQUFTLEVBQUMsQ0FBQyxFQUFFLENBQUM7SUFDM0MsTUFBTSw2RUFDSEMsQ0FBRztRQUFDQyxTQUFTLEVBQUMsQ0FBa0c7OEZBQzlHRCxDQUFHO1lBQUNDLFNBQVMsRUFBQyxDQUFzQjtrR0FDbENILFNBQVM7bUJBQUtDLFNBQVM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJaEMsQ0FBQztBQUVELEtBQUssQ0FBQ1oseUJBQXlCLElBQUk7Ozs7Ozs7O0FBUW5DIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vMjAyMi0wMS0yMS1zdXNwZW5zZS1lcnJvci1ib3VuZGFyaWVzLy4vc3JjL3BhZ2VzL19hcHAuanM/OGZkYSJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XG5pbXBvcnQgeyB1c2VFZmZlY3QsIHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBTV1JDb25maWcgfSBmcm9tIFwic3dyXCI7XG5pbXBvcnQgXCJ0YWlsd2luZGNzcy90YWlsd2luZC5jc3NcIjtcbmltcG9ydCBcIi4uLy4uL21pcmFnZVwiO1xuXG5sZXQgaXNDbGllbnQgPSB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiO1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBBcHAocHJvcHMpIHtcbiAgbGV0IFtpc0ZpcnN0UmVuZGVyLCBzZXRJc0ZpcnN0UmVuZGVyXSA9IHVzZVN0YXRlKHRydWUpO1xuXG4gIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgc2V0SXNGaXJzdFJlbmRlcihmYWxzZSk7XG4gIH0sIFtdKTtcblxuICByZXR1cm4gKFxuICAgIDw+XG4gICAgICA8SGVhZD5cbiAgICAgICAge3Byb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiAmJiAoXG4gICAgICAgICAgPHNjcmlwdFxuICAgICAgICAgICAgZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e3sgX19odG1sOiBub092ZXJsYXlXb3JrYXJvdW5kU2NyaXB0IH19XG4gICAgICAgICAgLz5cbiAgICAgICAgKX1cbiAgICAgIDwvSGVhZD5cblxuICAgICAge2lzQ2xpZW50ICYmICFpc0ZpcnN0UmVuZGVyID8gKFxuICAgICAgICA8U1dSQ29uZmlnXG4gICAgICAgICAgdmFsdWU9e3tcbiAgICAgICAgICAgIGZldGNoZXI6ICguLi5hcmdzKSA9PlxuICAgICAgICAgICAgICBmZXRjaCguLi5hcmdzKS50aGVuKChyZXMpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAocmVzLm9rKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gcmVzLmpzb24oKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiRmV0Y2ggZmFpbGVkXCIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxBcHBJbm5lciB7Li4ucHJvcHN9IC8+XG4gICAgICAgIDwvU1dSQ29uZmlnPlxuICAgICAgKSA6IG51bGx9XG4gICAgPC8+XG4gICk7XG59XG5cbmZ1bmN0aW9uIEFwcElubmVyKHsgQ29tcG9uZW50LCBwYWdlUHJvcHMgfSkge1xuICByZXR1cm4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBqdXN0aWZ5LWNlbnRlciB3LWZ1bGwgbWluLWgtc2NyZWVuIHB0LTggYW50aWFsaWFzZWQgeHM6cHgtMjAgc206cHgtMTAgbWQ6cHQtMjggYmctc2xhdGUtMTAwXCI+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInctZnVsbCBtYXgtdy1zbSBweC04XCI+XG4gICAgICAgIDxDb21wb25lbnQgey4uLnBhZ2VQcm9wc30gLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xufVxuXG5jb25zdCBub092ZXJsYXlXb3JrYXJvdW5kU2NyaXB0ID0gYFxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignZXJyb3InLCBldmVudCA9PiB7XG4gICAgZXZlbnQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKClcbiAgfSlcblxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigndW5oYW5kbGVkcmVqZWN0aW9uJywgZXZlbnQgPT4ge1xuICAgIGV2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpXG4gIH0pXG5gO1xuIl0sIm5hbWVzIjpbIkhlYWQiLCJ1c2VFZmZlY3QiLCJ1c2VTdGF0ZSIsIlNXUkNvbmZpZyIsImlzQ2xpZW50IiwiQXBwIiwicHJvcHMiLCJpc0ZpcnN0UmVuZGVyIiwic2V0SXNGaXJzdFJlbmRlciIsInNjcmlwdCIsImRhbmdlcm91c2x5U2V0SW5uZXJIVE1MIiwiX19odG1sIiwibm9PdmVybGF5V29ya2Fyb3VuZFNjcmlwdCIsInZhbHVlIiwiZmV0Y2hlciIsImFyZ3MiLCJmZXRjaCIsInRoZW4iLCJyZXMiLCJvayIsImpzb24iLCJFcnJvciIsIkFwcElubmVyIiwiQ29tcG9uZW50IiwicGFnZVByb3BzIiwiZGl2IiwiY2xhc3NOYW1lIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/pages/_app.js\n");

/***/ }),

/***/ "./node_modules/tailwindcss/tailwind.css":
/*!***********************************************!*\
  !*** ./node_modules/tailwindcss/tailwind.css ***!
  \***********************************************/
/***/ (() => {



/***/ }),

/***/ "faker":
/*!************************!*\
  !*** external "faker" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("faker");

/***/ }),

/***/ "miragejs":
/*!***************************!*\
  !*** external "miragejs" ***!
  \***************************/
/***/ ((module) => {

"use strict";
module.exports = require("miragejs");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "swr":
/*!**********************!*\
  !*** external "swr" ***!
  \**********************/
/***/ ((module) => {

"use strict";
module.exports = require("swr");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./src/pages/_app.js"));
module.exports = __webpack_exports__;

})();