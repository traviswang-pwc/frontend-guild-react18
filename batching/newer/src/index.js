import { useState, useLayoutEffect } from "react";
import * as ReactDOM from "react-dom/client";
import { flushSync} from 'react-dom'

function App() {
  const [count, setCount] = useState(0);
  const [flag, setFlag] = useState(false);

  function handleClick() {
    console.log("=== click ===");
    fetchSomething().then(() => {
      // React 18 with createRoot batches these:
      // flushSync(() => {
        setCount((c) => c + 1); // Does not re-render yet
      //})
      //setCount((c) => c + 1); // Does not re-render yet
      setFlag((f) => !f); // Does not re-render yet
      // React will only re-render once at the end (that's batching!)
    });
  }

  return (
    <div>
      <button onClick={handleClick}>Next</button>
      <h1 style={{ color: flag ? "blue" : "black" }}>{count}</h1>
      <LogEvents />
    </div>
  );
}

function LogEvents(props) {
  useLayoutEffect(() => {
    console.log("Commit");
  });
  console.log("Render");
  return null;
}

function fetchSomething() {
  return new Promise((resolve) => setTimeout(resolve, 100));
}

const rootElement = document.getElementById("root");
// This opts into the new behavior!
ReactDOM.createRoot(rootElement).render(<App />);
